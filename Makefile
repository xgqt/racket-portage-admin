# This file is part of racket-portage-admin.

# racket-portage-admin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-portage-admin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-portage-admin.	 If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


MAKE            := make
RACKET          := racket
RACO            := raco
SCRIBBLE        := $(RACO) scribble
SH              := sh

# For recursive calls
WHAT            :=

SRCDIR          := $(PWD)/src
ENTRYPOINT      := $(SRCDIR)/portage-admin-lib/portage-admin/main.rkt
APP-LAUNCHER    := $(PWD)/extras/racket-portage-admin.desktop
APPS-DIR        := ${HOME}/.local/share/applications


.PHONY: all src-make clean compile install setup test remove purge
.PHONY: public-clean public-regen
.PHONY: run-gui-main run-fresh

all: compile


src-make:
	cd $(SRCDIR) && $(MAKE) DEPS-FLAGS=" --no-pkg-deps " $(WHAT)

rmr:
	if [ -d $(WHAT) ] ; then rm -r $(WHAT) ; fi


clean:
	$(MAKE) src-make WHAT=clean
	$(MAKE) rmr WHAT=$(PWD)/bin

compile:
	$(MAKE) src-make WHAT=compile

install-launcher:
	mkdir -p $(APPS-DIR)
	cp $(APP-LAUNCHER) $(APPS-DIR)

install-racket:
	$(MAKE) src-make WHAT=install

install: install-racket install-launcher

setup:
	$(MAKE) src-make WHAT=setup

test:
	$(MAKE) src-make WHAT=test

remove:
	$(MAKE) src-make WHAT=remove

purge:
	$(MAKE) src-make WHAT=purge


bin:
	mkdir -p $(PWD)/bin
	$(RACO) exe -o $(PWD)/bin/portage-admin -v $(ENTRYPOINT)


public:
	$(SH) ./scripts/public.sh

public-clean:
	rm -dfr ./public

public-regen: public-clean public


run-gui-main:
	$(RACKET) $(ENTRYPOINT)

run-fresh: clean compile run-gui-main
