# Racket Portage Admin


## About

A GUI application for inspection of Gentoo-based systems.


## Screenshots

![version 1.3.0](extras/images/screenshots/1.3.0.png "version 1.3.0")


## Installation

To install, execute:
```bash
make install
```

### Dependencies

- threading-lib

### Subpackages

| name               | role                               |
|--------------------|------------------------------------|
| portage-admin      | metapackage                        |
| portage-admin-doc  | documentation of the whole project |
| portage-admin-lib  | core library                       |
| portage-admin-test | tests                              |

To install a subpackage go it it's directory and execute `raco pkg install`.


## Running

To run without installation ensure you have
the dependencies installed (listed above),
and execute:
```bash
make compile
sh ./scripts/main.sh
```


## License

SPDX-License-Identifier: GPL-3.0-only

This file is part of racket-portage-admin .

racket-portage-admin  is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

racket-portage-admin  is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with racket-portage-admin .  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
