#!/usr/bin/env sh


# This file is part of racket-portage-admin.

# racket-portage-admin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-portage-admin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-portage-admin.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


trap 'exit 128' INT
set -e
export PATH


myroot="$( realpath "$( dirname "${0}" )/../" )"


cd "${myroot}/docs"

sh ./html.sh

cp -r ./html/main "${myroot}/public"
