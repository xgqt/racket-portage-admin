#!/usr/bin/env racket


;; This file is part of racket-portage-admin.

;; racket-portage-admin is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-portage-admin is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-portage-admin.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base

(require
 racket/gui
 plot
 "../../pkgdb.rkt")

(provide load-gui)


(define (count-attribute-values attr pkgs-list)
  ; pre-loaded pkgdb in order to not call pkgdb function all the time (in lambda)
  (define db (pkgdb))
  (define attrs
    (map (lambda (pkg) (attribute-file-contents attr pkg db)) pkgs-list))
  (for/list ([attr (in-set (list->set attrs))])
    (vector attr (count (lambda (str) (equal? str attr)) attrs))))

(define (graph-attribute-values attr pkgs-list)
  (parameterize ([plot-new-window? #t]
                 [plot-x-label "value"]
                 [plot-y-label "occurrences"])
    (plot (discrete-histogram #:label attr
                              (count-attribute-values attr pkgs-list)))))


(define (load-gui)

  (define all-pkgs (delay/thread (get-all-pkgs)))

  (define selected-attribute (car pkg-attributes))

  (define main-frame
    (new frame%
         [label "Package Database Statistics"]
         [min-width 300]
         [min-height 120]))

  (define attributes-combo-field
    (new choice%
         [parent main-frame]
         [label "Attribute: "]
         [choices pkg-attributes]
         [vert-margin 20]
         [callback
          (lambda (o e)
            (set! selected-attribute
                  (list-ref pkg-attributes (send o get-selection)))
            (graph-attribute-values selected-attribute (force all-pkgs)))]))

  (define main-horizontal-panel
    (new horizontal-panel%
         [parent main-frame]
         [alignment '(center center)]
         [stretchable-height #f]))

  (define show-botton
    (new button%
         [parent main-horizontal-panel]
         [label "Show"]
         [callback
          (λ _ (graph-attribute-values selected-attribute (force all-pkgs)))]))

  (define close-botton
    (new button%
         [parent main-horizontal-panel]
         [label "Close"]
         [callback (λ _ (send main-frame show #f))]))

  main-frame)


(module+ main
  (send (load-gui) show #t)
  )
