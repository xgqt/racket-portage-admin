#!/usr/bin/env racket


;; This file is part of racket-portage-admin.

;; racket-portage-admin is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-portage-admin is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-portage-admin.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base

(require
 racket/gui
 racket/string
 threading
 "builds.rkt"
 "command-message.rkt"
 "terminal.rkt"
 "spawn/frame.rkt"
 "spawn/subprocess.rkt"
 (prefix-in e/e/ "explorer/eselect/main.rkt")
 (prefix-in e/l/ "explorer/log/main.rkt")
 (prefix-in e/p/ "explorer/pkgdb/main.rkt")
 (prefix-in s/p/ "stats/pkgdb/main.rkt"))

(provide
 main/load-gui
 e/e/load-gui
 e/l/load-gui
 e/p/load-gui)


(define portage-conf-path
  (make-parameter (string-append (or (getenv "EROOT") "") "/etc/portage")))


(define (line-group parent label)
  (define group
    (new group-box-panel%
         [parent parent]
         [label label]
         [alignment '(left top)]
         [stretchable-height #f]))
  (new horizontal-panel%
       [parent group]
       [alignment '(center top)]
       [stretchable-height #f]))


(define (main/load-gui)

  (define s-f (new spawned-frames%))
  (define s-p (new spawned-subprocesses%))

  (define main-frame
    (new frame%
         [label "Portage Administrator"]
         [min-width 700]
         [min-height 700]))

  ;; Tear-down

  (define (cleanup)
    (send s-f close-spawned-frames)
    (send s-p kill-spawned-subprocesses))

  (define (close-main-frame . _)
    (displayln (make-string 30 #\-))
    (displayln "Exitng...")
    (cleanup)
    (send main-frame show #f)
    (displayln "Bye!")
    (displayln (make-string 30 #\-)))

  ;; Menu bar

  (define main-menu-bar
    (new menu-bar%
         [parent main-frame]))

  (define menu-file
    (new menu%
         [parent main-menu-bar]
         [label "&File"]))

  (define menu-item-quit
    (new menu-item%
         [label "&Quit"]
         [parent menu-file]
         [callback close-main-frame]
         [shortcut #\q]))

  ;; Explorers

  (define explorers-lg (line-group main-frame "Explorers"))

  (define spawn-eselect-explorer-button
    (new button%
         [parent explorers-lg]
         [label "Eselect Modules"]
         [callback
          (lambda _
            (delay/thread (send s-f spawn-new-frame (e/e/load-gui))))]))

  (define spawn-pkgdb-explorer-button
    (new button%
         [parent explorers-lg]
         [label "Package Database"]
         [callback
          (lambda _
            (delay/thread (send s-f spawn-new-frame (e/p/load-gui))))]))

  (define spawn-logs-explorer-button
    (new button%
         [parent explorers-lg]
         [label "Portage Logs"]
         [callback
          (lambda _
            (delay/thread (send s-f spawn-new-frame (e/l/load-gui))))]))

  ;; Statistics

  (define statistics-lg (line-group main-frame "Statistics"))

  (define spawn-pkgdb-stats-button
    (new button%
         [parent statistics-lg]
         [label "Package Database"]
         [callback
          (lambda _
            (delay/thread (send s-f spawn-new-frame (s/p/load-gui))))]))

  ;; External programs

  (define external-lg (line-group main-frame "External Programs"))

  (define spawn-log-reader-button
    (new button%
         [parent external-lg]
         [label "Bash"]
         [callback
          (lambda _
            (send s-p
                  capture-new-subprocess
                  (exec-in-default-terminal
                   (format "sh -c 'cd ~a && bash'" (portage-conf-path)))))]))

  (define spawn-emacs-button
    (new button%
         [parent external-lg]
         [label "Emacs"]
         [callback
          (lambda _
            (send s-p
                  capture-new-subprocess
                  (exec-in-default-terminal
                   (format "emacs -nw ~a" (portage-conf-path)))))]))

  (define spawn-process-monitor-button
    (new
     button%
     [parent external-lg]
     [label "System process monitor (htop)"]
     [callback
      (lambda _
        (send s-p capture-new-subprocess (exec-in-default-terminal "htop")))]))

  ;; Portage's emerge progress

  (define emerge-lg (line-group main-frame "Emerge"))

  (define emerge-pretend-text-field-args
    (new text-field%
         [parent emerge-lg]
         [label "Arguments: "]
         [min-width 200]
         [init-value "-pv"]))

  (define emerge-pretend-text-field-pkg
    (new text-field%
         [parent emerge-lg]
         [label "Package: "]
         [min-width 200]
         [init-value "dev-scheme/racket"]))

  (define emerge-pretend-button
    (new button%
         [parent emerge-lg]
         [label "GO"]
         [callback
          (lambda _
            (define a (send emerge-pretend-text-field-args get-value))
            (define w (send emerge-pretend-text-field-pkg get-value))
            (delay/thread
             (send s-f spawn-new-frame (command-message "emerge" a w))))]))

  ;; Updates

  (define update-lg (line-group main-frame "Update"))

  (define emerge-sync-button
    (new button%
         [parent update-lg]
         [label "Repositories"]
         [callback
          (lambda _
            (delay/thread (send s-f
                                spawn-new-frame
                                (command-message "emerge" "--sync"))))]))

  (define egencache-button
    (new button%
         [parent update-lg]
         [label "Ebuild Cache"]
         [callback
          (lambda _
            (delay/thread (send s-f
                                spawn-new-frame
                                (command-message "egencache"
                                                 "--verbose"
                                                 "--update"
                                                 "--repo"
                                                 "gentoo"))))]))

  (define eix-update-button
    (new button%
         [parent update-lg]
         [label "EIX Cache"]
         [callback
          (lambda _
            (delay/thread
             (send s-f spawn-new-frame (command-message "eix-update"))))]))

  (define builds-group-box-panel
    (new group-box-panel%
         [parent main-frame]
         [label "Currently running builds"]
         [alignment '(left top)]
         [stretchable-height #t]))

  (define builds-list-editor-canvas
    (new text-field%
         [parent builds-group-box-panel]
         [label ""]
         [min-height 50]
         [stretchable-height #f]
         [style '(multiple)]))
  (define builds-list-txt (send builds-list-editor-canvas get-editor))

  (define builds-log-editor-canvas
    (new text-field%
         [parent builds-group-box-panel]
         [label ""]
         [min-height 200]
         [stretchable-height #t]
         [style '(multiple)]))
  (define builds-log-txt (send builds-log-editor-canvas get-editor))

  (delay/thread
   (let loop ()
     (let* ([builds (portage-tmpdir-builds)]
            [t1 (delay/thread
                 (send builds-list-txt begin-edit-sequence #t #f)
                 (send builds-list-txt erase)
                 (if (null? builds)
                     (send builds-list-txt insert "none")
                     (send builds-list-txt insert (string-join builds " ")))
                 (send builds-list-txt end-edit-sequence))]
            [t2 (delay/thread
                 (send builds-list-txt begin-edit-sequence #t #f)
                 (send builds-log-txt erase)
                 (~> builds
                     (map
                      (lambda (b)
                        (build-path (portage-tmpdir-path) b "temp/build.log"))
                      _)
                     (filter file-exists? _)
                     (map file->lines _)
                     (filter (lambda (lst) (> (length lst) 25)) _)
                     (map (lambda (lst) (take-right lst 25)) _)
                     (apply append _)
                     (string-join _ "\n")
                     (send builds-log-txt insert _))
                 (send builds-list-txt end-edit-sequence))])
       (map sync (list t1 t2))
       (sleep/yield 5)
       (loop))))

  (define close-button
    (new button%
         [parent main-frame]
         [label "Close"]
         [callback close-main-frame]))

  main-frame)


(module+ main
  (send (main/load-gui) show #t)
  )
