#lang racket/base

(provide (all-defined-out))


(define-values (MAJOR MINOR PATCH) (values 1 3 3))

(define VERSION (format "~a.~a.~a" MAJOR MINOR PATCH))


(define (echo-version program [version VERSION])
  (printf "\"~a\" is a part of Portage-Admin, version ~a\n" program version))
